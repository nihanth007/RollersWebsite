﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Rollers Technology Solutions</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="Free HTML5 Template by FREEHTML5.CO" />
    <meta name="keywords" content="free html5, free template, free bootstrap, html5, css3, mobile first, responsive" />
    <meta name="author" content="FREEHTML5.CO" />
    <meta property="og:title" content="" />
    <meta property="og:image" content="" />
    <meta property="og:url" content="" />
    <meta property="og:site_name" content="" />
    <meta property="og:description" content="" />
    <meta name="twitter:title" content="" />
    <meta name="twitter:image" content="" />
    <meta name="twitter:url" content="" />
    <meta name="twitter:card" content="" />
    <link rel="shortcut icon" href="favicon.ico" />
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700|Monsterrat:400,700|Merriweather:400,300italic,700' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="css/animate.css" />
    <link rel="stylesheet" href="css/icomoon.css" />
    <link rel="stylesheet" href="css/magnific-popup.css" />
    <link rel="stylesheet" href="css/owl.carousel.min.css" />
    <link rel="stylesheet" href="css/owl.theme.default.min.css" />
    <link rel="stylesheet" href="css/bootstrap.css" />
    <link rel="stylesheet" href="css/cards.css" />
    <script src="js/modernizr-2.6.2.min.js"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <form id="form1" runat="server">

        <div id="fh5co-page">
            <nav class="fh5co-nav-style-1" role="navigation" data-offcanvass-position="fh5co-offcanvass-left">
                <div class="container">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 fh5co-logo">
                        <a href="#" class="js-fh5co-mobile-toggle fh5co-nav-toggle"><i></i></a>
                        <a href="#">Rollers Technology Solutions</a>
                    </div>
                    <div class="col-lg-6 col-md-5 col-sm-5 text-center fh5co-link-wrap">
                        <ul data-offcanvass="yes">
                            <li class="active"><a href="#">Home</a></li>
                            <li><a href="#">Explore</a></li>
                            <li><a href="#">Team</a></li>
                            <li><a href="#">Pricing</a></li>
                        </ul>
                    </div>
                    <!--
                <div class="col-lg-3 col-md-4 col-sm-4 text-right fh5co-link-wrap">
                    <ul class="fh5co-special" data-offcanvass="yes">
                        <li><a href="#">Login</a></li>
                        <li><a href="#" class="call-to-action">Get Started</a></li>
                    </ul>
                </div>-->
                </div>
            </nav>

            <div class="fh5co-cover fh5co-cover-style-2 js-full-height" data-stellar-background-ratio="0.5" data-next="yes" style="background-image: url(images/full_1.jpg);">
                <span class="scroll-btn wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.4s">
                    <a href="#">
                        <span class="mouse"><span></span></span>
                    </a>
                </span>
                <div class="fh5co-overlay"></div>
                <div class="fh5co-cover-text">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-push-6 col-md-6 full-height js-full-height">
                                <div class="fh5co-cover-intro">
                                    <h1 class="cover-text-lead wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s">Create Amazing Technologies</h1>
                                    <h2 class="cover-text-sublead wow fadeInUp" data-wow-duration="1s" data-wow-delay=".8s">Welcome to the world of technologies.<br>
                                        We are here to make you technical dreams come true.</h2>
                                    <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.1s"><a href="http://freehtml5.co/" class="btn btn-primary btn-outline btn-lg">See Our Work</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="fh5co-project-style-2">
                <div class="container">
                    <div class="row p-b">
                        <div class="col-md-6 col-md-offset-3 text-center">
                            <h2 class="fh5co-heading wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s">Our Projects</h2>
                            <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".8s">
                                Technologies grow day by day.<br>
                                But being a developer we grow with the Technologies.<br>
                                Rollers Technology Solutions is here to Develop Web Technologies for commercial usage<br>
                                Here are Our projects till date.
                            </p>
                            <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.1s"><a href="#" class="btn btn-success btn-lg">Have a Glance</a></p>
                        </div>
                    </div>
                </div>
                <div class="fh5co-projects">
                    <ul>
                        <li class="wow fadeInUp" style="background-image: url(images/full_4.jpg);" data-wow-duration="1s" data-wow-delay="1.4s" data-stellar-background-ratio="0.5">
                            <a href="http://mystiquestudio.in/">
                                <div class="fh5co-overlay"></div>
                                <div class="container">
                                    <div class="fh5co-text">
                                        <div class="fh5co-text-inner">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <h3>MystiqueStudio</h3>
                                                </div>
                                                <div class="col-md-6">
                                                    <p>Event organisers have come in search of a website which would attract the customers.Smart working team of rollers have created a Website which made their eyes look up.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="wow fadeInUp" style="background-image: url(images/full_2.jpg);" data-wow-duration="1s" data-wow-delay="1.7s" data-stellar-background-ratio="0.5">
                            <a href="http://www.margam.in/">
                                <div class="fh5co-overlay"></div>
                                <div class="container">
                                    <div class="fh5co-text">
                                        <div class="fh5co-text-inner">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <h3>Margam Foundation Trust</h3>
                                                </div>
                                                <div class="col-md-6">
                                                    <p>
                                                        Website is not only the heart of a company.<br>
                                                        It maintains the same importance For a socail Organisation.Rollers have developed a website for this non-governament organisation
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="wow fadeInUp" style="background-image: url(images/full_1.jpg);" data-wow-duration="1s" data-wow-delay="2s" data-stellar-background-ratio="0.5">
                            <a href="http://globalmars.in/">
                                <div class="fh5co-overlay"></div>
                                <div class="container">
                                    <div class="fh5co-text">
                                        <div class="fh5co-text-inner">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <h3>Global Mars</h3>
                                                </div>
                                                <div class="col-md-6">
                                                    <p>
                                                        Being a Professional Developers We encourage saftey first policy.<br>
                                                        Rollers have developed a Innovative Website for this firm with charming features.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            

            <div class="fh5co-blog-style-1">
                <div class="container">
                    <div class="row p-b">
                        <div class="col-md-6 col-md-offset-3 text-center">
                            <h2 class="fh5co-heading wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s">Recent From Blog</h2>
                            <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".8s">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                        </div>
                    </div>
                    <div class="row p-b">
                        <div class="col-md-4 col-sm-6 col-xs-6 col-xxs-12">
                            <div class="fh5co-post wow fadeInLeft"  data-wow-duration="1s" data-wow-delay="1.1s">
                                <div class="fh5co-post-image">
                                    <div class="fh5co-overlay"></div>
                                    <div class="fh5co-category"><a href="#">Tutorial</a></div>
                                    <img src="images/img_same_dimension_2.jpg" alt="Image" class="img-responsive">
                                </div>
                                <div class="fh5co-post-text">
                                    <h3><a href="#">How to Create Cards</a></h3>
                                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts...</p>
                                </div>
                                <div class="fh5co-post-meta">
                                    <a href="#"><i class="icon-chat"></i> 33</a>
                                    <a href="#"><i class="icon-clock2"></i> 2 hours ago</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-6 col-xxs-12">
                            <div class="fh5co-post wow fadeInLeft"  data-wow-duration="1s" data-wow-delay="1.4s">
                                <div class="fh5co-post-image">
                                    <div class="fh5co-overlay"></div>
                                    <div class="fh5co-category"><a href="#">Health</a></div>
                                    <img src="images/img_same_dimension_3.jpg" alt="Image" class="img-responsive">
                                </div>
                                <div class="fh5co-post-text">
                                    <h3><a href="#">Drinking Ginger and Lemon Tea</a></h3>
                                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts...</p>
                                </div>
                                <div class="fh5co-post-meta">
                                    <a href="#"><i class="icon-chat"></i> 33</a>
                                    <a href="#"><i class="icon-clock2"></i> 2 hours ago</a>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix visible-sm-block"></div>
                        <div class="col-md-4 col-sm-6 col-xs-6 col-xxs-12">
                            <div class="fh5co-post wow fadeInLeft"  data-wow-duration="1s" data-wow-delay="1.7s">
                                <div class="fh5co-post-image">
                                    <div class="fh5co-overlay"></div>
                                    <div class="fh5co-category"><a href="#">Tips</a></div>
                                    <img src="images/img_same_dimension_4.jpg" alt="Image" class="img-responsive">
                                </div>
                                <div class="fh5co-post-text">
                                    <h3><a href="#">4 Easy Steps to Create a Soup</a></h3>
                                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts...</p>
                                </div>
                                <div class="fh5co-post-meta">
                                    <a href="#"><i class="icon-chat"></i> 33</a>
                                    <a href="#"><i class="icon-clock2"></i> 2 hours ago</a>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix visible-sm-block"></div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-md-offset-4 text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay="2s">
                            <a href="#" class="btn btn-primary btn-lg">View All Post</a>
                        </div>
                    </div>
                </div>
            </div>
        

            <div class="fh5co-features-style-1" style="background-image: url(images/full_4.jpg);" data-stellar-background-ratio="0.5">
                <div class="fh5co-overlay"></div>
                <div class="container" style="z-index: 3; position: relative;">
                    <div class="row p-b">
                        <div class="col-md-6 col-md-offset-3 text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s">
                            <h2 class="fh5co-heading">Here's what we do</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="fh5co-features">
                            <div class="fh5co-feature wow fadeInUp" data-wow-duration="1s" data-wow-delay=".8s">
                                <div class="icon"><i class="icon-ribbon"></i></div>
                                <h3>Web Site Development</h3>
                                <p>We develop Static and dynamic websites with with additional featuer for low cost</p>
                            </div>
                            <div class="fh5co-feature wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.1s">
                                <div class="icon"><i class="icon-image22"></i></div>
                                <h3>Multimedia</h3>
                                <p>Team Rollers has also entered a different talent of multimedia.</p>
                            </div>
                            <div class="fh5co-feature wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.4s">
                                <div class="icon"><i class=" icon-monitor"></i></div>
                                <h3>Search engine Optimisation</h3>
                                <p>The process of maximizing the number of visitors to a particular website by ensuring that the site appears high on the list of results returned by a search engine..</p>
                            </div>
                            <div class="fh5co-feature wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.7s">
                                <div class="icon"><i class="icon-video2"></i></div>
                                <h3>Mobile Applications</h3>
                                <p>Smart phones are the most used technology.Android is being the most spreading software ever.We step forward in developing flash Apps.</p>
                            </div>
                            <div class="fh5co-feature wow fadeInUp" data-wow-duration="1s" data-wow-delay="2s">
                                <div class="icon"><i class="icon-bag"></i></div>
                                <h3>Training & Internships</h3>
                                <p>Learning is the most important process for a software student and Real time knowledge and experiance makes a lot change.Rollers offer low cost training and internships</p>
                            </div>
                            <div class="fh5co-feature wow fadeInUp" data-wow-duration="1s" data-wow-delay="2.3s">
                                <div class="icon"><i class="icon-mail2"></i></div>
                                <h3>Invoice Solutions Hardware and Software Services</h3>
                                <p>We offer Low cost invoice solutions and we have professional Hardware engineers and also software developers.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--
                <div class="fh5co-features-style-2">
                    <div class="container">
                        <div class="row p-b">
                            <div class="col-md-6 col-md-offset-3 text-center">
                                <h2 class="fh5co-heading wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s">Why Choose Us</h2>
                                <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".8s">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-xs-6 col-xxs-12 fh5co-feature wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.1s">
                            <div class="fh5co-icon"><i class="icon-command"></i></div>
                            <div class="fh5co-desc">
                                <h3>100% Free</h3>
                                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-6 col-xxs-12 fh5co-feature wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.4s">
                            <div class="fh5co-icon"><i class="icon-eye22"></i></div>
                            <div class="fh5co-desc">
                                <h3>Retina Ready</h3>
                                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                            </div>
                        </div>
                        <div class="clearfix visible-sm-block visible-xs-block"></div>
                        <div class="col-md-4 col-sm-6 col-xs-6 col-xxs-12 fh5co-feature wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.7s">
                            <div class="fh5co-icon"><i class="icon-toggle"></i></div>
                            <div class="fh5co-desc">
                                <h3>Fully Responsive</h3>
                                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-6 col-xxs-12 fh5co-feature wow fadeInUp" data-wow-duration="1s" data-wow-delay="2s">
                            <div class="fh5co-icon"><i class="icon-archive22"></i></div>
                            <div class="fh5co-desc">
                                <h3>Lightweight</h3>
                                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                            </div>
                        </div>
                        <div class="clearfix visible-sm-block visible-xs-block"></div>
                        <div class="col-md-4 col-sm-6 col-xs-6 col-xxs-12 fh5co-feature wow fadeInUp" data-wow-duration="1s" data-wow-delay="2.3s">
                            <div class="fh5co-icon"><i class="icon-heart22"></i></div>
                            <div class="fh5co-desc">
                                <h3>Made with Love</h3>
                                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-6 col-xxs-12 fh5co-feature wow fadeInUp" data-wow-duration="1s" data-wow-delay="2.6s">
                            <div class="fh5co-icon"><i class="icon-umbrella"></i></div>
                            <div class="fh5co-desc">
                                <h3>Eco Friendly</h3>
                                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                            </div>
                        </div>
                        <div class="clearfix visible-sm-block visible-xs-block"></div>
                        </div>
                    </div>
                </div>
        -->
            <div class="fh5co-pricing-style-2">
                <div class="container">
                    <div class="row">
                        <div class="row p-b">
                            <div class="col-md-6 col-md-offset-3 text-center">
                                <h2 class="fh5co-heading wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s">Pricing Plans</h2>
                                <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".7s">
                                    We provide a low cost developments.Here are our few pricing plans<br>
                                    please do contact us for more.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="pricing">
                            <div class="pricing-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.1s">
                                <h3 class="pricing-title">Static applications</h3>
                                <div class="pricing-price"><span class="pricing-currency">Rs</span>5K<span class="pricing-period"></span></div>
                                <ul class="pricing-feature-list">
                                    <li class="pricing-feature">Free website developement</li>
                                    <li class="pricing-feature">1 month free maintance</li>
                                    <li class="pricing-feature">8 hrs turn around time</li>
                                </ul>
                                <button class="btn btn-success btn-outline">Choose plan</button>
                            </div>
                            <div class="pricing-item pricing-item--featured wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.4s">
                                <h3 class="pricing-title">Rollers Special</h3>
                                <p class="pricing-sentence"></p>
                                <div class="pricing-price"><span class="pricing-currency">Rs</span>10K<span class="pricing-period"></span></div>
                                <ul class="pricing-feature-list">
                                    <li class="pricing-feature">3 Free Websites.</li>
                                    <li class="pricing-feature">Seaarch Engine Optimization</li>
                                    <li class="pricing-feature">2 months free maintanace</li>
                                </ul>
                                <button class="btn btn-success btn-lg">Choose plan</button>
                            </div>
                            <div class="pricing-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.7s">
                                <h3 class="pricing-title">Enterprise</h3>
                                <p class="pricing-sentence">Certificate validation</p>
                                <div class="pricing-price"><span class="pricing-currency">Rs</span>15K<span class="pricing-period"></span></div>
                                <ul class="pricing-feature-list">
                                    <li class="pricing-feature">2 free websites</li>
                                    <li class="pricing-feature">Dedicated for training institutes</li>
                                    <li class="pricing-feature">dedicated project manager</li>
                                </ul>
                                <button class="btn btn-success btn-outline">Choose plan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--
        <div class="fh5co-content-style-6">
            <div class="container">
                <div class="row p-b text-center">
                    <div class="col-md-6 col-md-offset-3">
                        <h2 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s">Explore the impossibility.</h2>
                        <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".8s">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".5s">
                        <a href="#" class="link-block">
                            <figure><img src="images/img_same_dimension_1.jpg" alt="Image" class="img-responsive img-rounded"></figure>
                            <h3>Responsive Layout</h3>
                            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                            <p><a href="#" class="btn btn-primary btn-sm">Learn More</a></p>
                        </a>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".8s">
                        <a href="#" class="link-block">
                            <figure><img src="images/img_same_dimension_2.jpg" alt="Image" class="img-responsive img-rounded"></figure>
                            <h3>Retina Ready</h3>
                            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                            <p><a href="#" class="btn btn-primary btn-sm">Learn More</a></p>
                        </a>
                    </div>
                    <div class="clearfix visible-sm-block"></div>
                    <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="1.1s">
                        <a href="#" class="link-block">
                            <figure><img src="images/img_same_dimension_3.jpg" alt="Image" class="img-responsive img-rounded"></figure>
                            <h3>Creative UI Design</h3>
                            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                            <p><a href="#" class="btn btn-primary btn-sm">Learn More</a></p>
                        </a>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6 col-xxs-12 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="1.4s">
                        <a href="#" class="link-block">
                            <figure><img src="images/img_same_dimension_4.jpg" alt="Image" class="img-responsive img-rounded"></figure>
                            <h3>Responsive Layout</h3>
                            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                            <p><a href="#" class="btn btn-primary btn-sm">Learn More</a></p>
                        </a>
                    </div>
                </div>
            </div>
        </div> -->
            <div class="fh5co-counter-style-2" style="background-image: url(images/full_2.jpg);" data-stellar-background-ratio="0.5">
                <div class="fh5co-overlay"></div>
                <div class="container">
                    <div class="fh5co-section-content-wrap">
                        <div class="fh5co-section-content">
                            <div class="row">
                                <div class="col-md-4 text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s">
                                    <div class="icon">
                                        <i class="icon-command"></i>
                                    </div>
                                    <span class="fh5co-counter js-counter" data-from="0" data-to="10" data-speed="5000" data-refresh-interval="50"></span>
                                    <span class="fh5co-counter-label">Clients Worked With</span>

                                </div>
                                <div class="col-md-4 text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay=".8s">
                                    <div class="icon">
                                        <i class="icon-power"></i>
                                    </div>
                                    <span class="fh5co-counter js-counter" data-from="0" data-to="5" data-speed="5000" data-refresh-interval="50"></span>
                                    <span class="fh5co-counter-label">Completed Projects</span>
                                </div>
                                <div class="col-md-4 text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.1s">
                                    <div class="icon">
                                        <i class="icon-code2"></i>
                                    </div>
                                    <span class="fh5co-counter js-counter" data-from="0" data-to="34023" data-speed="5000" data-refresh-interval="50"></span>
                                    <span class="fh5co-counter-label">Line of Codes</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--
            <div class="fh5co-testimonial-style-2">
                <div class="container">
                    <div class="row p-b">
                        <div class="col-md-6 col-md-offset-3 text-center">
                            <h2 class="fh5co-heading wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s">Happy Customer</h2>
                            <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".8s">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="fh5co-testimonial-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.1s">
                                <div class="fh5co-name">
                                    <img src="images/person_5.jpg" alt="">
                                    <div class="fh5co-meta">
                                        <h3>Chris Clark</h3>
                                        <span class="fh5co-company">XYZ Inc.</span>
                                    </div>
                                </div>
                                <div class="fh5co-text">
                                    <p>&ldquo;Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.&rdquo;</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="fh5co-testimonial-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.4s">
                                <div class="fh5co-name">
                                    <img src="images/person_4.jpg" alt="">
                                    <div class="fh5co-meta">
                                        <h3>Ian Stewart</h3>
                                        <span class="fh5co-company">XYZ Inc.</span>
                                    </div>
                                </div>
                                <div class="fh5co-text">
                                    <p>&ldquo;Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. &rdquo;</p>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix visible-sm-block"></div>

                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="fh5co-testimonial-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.7s">
                                <div class="fh5co-name">
                                    <img src="images/person_3.jpg" alt="">
                                    <div class="fh5co-meta">
                                        <h3>Mitch Silk</h3>
                                        <span class="fh5co-company">XYZ Inc.</span>
                                    </div>
                                </div>
                                <div class="fh5co-text">
                                    <p>&ldquo;Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.&rdquo;</p>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix visible-lg-block visible-md-block"></div>

                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="fh5co-testimonial-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="2s">
                                <div class="fh5co-name">
                                    <img src="images/person_1.jpg" alt="">
                                    <div class="fh5co-meta">
                                        <h3>Beth Glasgow</h3>
                                        <span class="fh5co-company">XYZ Inc.</span>
                                    </div>
                                </div>
                                <div class="fh5co-text">
                                    <p>&ldquo;Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. &rdquo;</p>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix visible-sm-block"></div>

                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="fh5co-testimonial-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="2.3s">
                                <div class="fh5co-name">
                                    <img src="images/person_2.jpg" alt="">
                                    <div class="fh5co-meta">
                                        <h3>Rob Smith</h3>
                                        <span class="fh5co-company">XYZ Inc.</span>
                                    </div>
                                </div>
                                <div class="fh5co-text">

                                    <p>&ldquo;Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.&rdquo;</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="fh5co-testimonial-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="2.6s">
                                <div class="fh5co-name">
                                    <img src="images/person_6.jpg" alt="">
                                    <div class="fh5co-meta">
                                        <h3>Colleen Bass</h3>
                                        <span class="fh5co-company">XYZ Inc.</span>
                                    </div>
                                </div>
                                <div class="fh5co-text">
                                    <p>&ldquo;Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.&rdquo;</p>
                                </div>
                            </div>
                        </div>
                        -->
            <div class="clearfix visible-sm-block"></div>
        </div>
        </div>
    </div>

    <div class="fh5co-footer-style-3">
        <div class="container">
            <div class="row p-b">
                <div class="col-md-3 col-sm-6 fh5co-footer-widget wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s">
                    <div class="fh5co-logo"><span class="logo">R</span> Rollers Technology Solutions</div>
                    <p class="fh5co-copyright">
                        &copy; 2017 Rollers Technology SOlutions.
                        <br>
                        All Rights Reserved.<br>
                </div>
                <div class="col-md-3 col-sm-6 fh5co-footer-widget wow fadeInUp" data-wow-duration="1s" data-wow-delay=".8s">
                    <h3>Company</h3>
                    <ul class="fh5co-links">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Services</a></li>
                        <li><a href="#">Products</a></li>
                        <!--<li><a href="#">Careers</a></li>
                        <li><a href="#">Blog</a></li> -->
                        <li><a href="#">Contact</a></li>
                    </ul>
                </div>
                <div class="clearfix visible-sm-block"></div>
                <div class="col-md-3 col-sm-6 fh5co-footer-widget wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.1s">
                    <h3>Connect</h3>
                    <ul class="fh5co-links fh5co-social">
                        <li><a href="https://www.facebook.com/rollersindiainc/?ref=br_rs"><i class="icon icon-facebook2"></i>Facebook</a></li>
                        <li><a href="#"><i class="icon icon-twitter"></i>Twitter</a></li>
                        <li><a href="#"><i class="icon icon-dribbble"></i>Dribbble</a></li>
                        <li><a href="https://www.instagram.com/rollerstechnologysolutions/?hl=en"><i class="icon icon-instagram"></i>Instagram</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-6 fh5co-footer-widget wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.4s">
                    <h3>About</h3>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                    <p><a href="#" class="btn btn-success btn-sm btn-outline">I'm a button</a></p>
                </div>
                <div class="clearfix visible-sm-block"></div>
            </div>
            <div class="row fh5co-made">
                <div class="col-md-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay=".5s">
                    <p>Made with <i class="heart icon-heart"></i>With Technology</p>
                </div>
            </div>
        </div>
    </div>
    </form>
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.stellar.min.js"></script>
    <script src="js/jquery.countTo.js"></script>
    <script src="js/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>
    <script src="js/main.js"></script>
</body>
</html>
